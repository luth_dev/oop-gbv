#include <iostream>
#include <string>
#include <bitset>

class NumarRational {
public:
    NumarRational() : m_nr(0) {}
    NumarRational(int x) : m_nr(x) {}

    void operator=(int n) {
        m_nr = n;
    }
    int operator()() {
        return m_nr;
    }
private:
    int m_nr;
};

class Carte {
public:
    Carte(std::string titlu, std::string autor, int pagini) : 
        m_titlu(titlu), m_autor(autor), m_nrPagini(pagini) {}
    
    std::string GetAutor() { return m_autor; }
    std::string GetTitlu() { return m_titlu; }
    int GetNrPagini() { return m_nrPagini; }
private:
    Carte() {}
    std::string m_titlu;
    std::string m_autor;
    int m_nrPagini;
};

class Student {
public:
    Student(std::string nume, std::string prenume, std::string facultate, int an = 1) : m_an(an) {
        m_nume = new char[nume.length()];
        m_prenume = new char[prenume.length()];
        m_facultate = new char[facultate.length()];
        nume.copy(m_nume, nume.length());
        prenume.copy(m_prenume, prenume.length());
        facultate.copy(m_facultate, facultate.length());
    }

    void print() {
        std::cout << m_nume << " " << m_prenume << " este la facultatea " << m_facultate << " in anul " << m_an << ".\n";
    }

    ~Student() {
        delete[] m_nume;
        delete[] m_prenume;
    }
private:
    char* m_nume;
    char* m_prenume;
    char* m_facultate;

    int m_an;
};

int main(int argc, char** argv) {
    return 0;
}