#include <iostream>
#include <cstring>

#define PI 3.14159265359

class Persoana {
public:
    Persoana(std::string nume, int varsta, int sex = 0) : m_varsta(varsta), m_sex(sex) {
        m_nume = new char[nume.length()+1];
        strcpy(m_nume, nume.c_str());
    }
    Persoana(const Persoana& p) {
        m_nume = new char[strlen(p.m_nume)+1];
        strcpy(m_nume, p.m_nume);
        m_sex = p.m_sex;
        m_varsta = p.m_varsta;
    }

    virtual void print() {
        std::cout << getNume() << " este " << getSex() << " " << getVarsta() << std::endl;
    }

    char* getNume() const {
        return m_nume;
    }

    int getVarsta() const {
        return m_varsta;
    }

    std::string getSex() const {
        if (m_sex == 1)
            return "un barbat";
        else if (m_sex == 2)
            return "o femeie";
        else
            return "sex nedefinit";
    }
private:
    char* m_nume;
    int m_varsta;
    int m_sex;
};

class Angajat : public Persoana {
public:
    Angajat(const Persoana& p, int salariu, int vechime = 0) : Persoana(p), m_salariu(salariu), m_vechime(vechime) { }
    Angajat(const Angajat& a) : Persoana(a) {
        m_salariu = a.m_salariu;
        m_vechime = a.m_vechime;
    }
    void print() override {
        Persoana::print();
        std::cout << "El este angajat de " << getVechime() << " si are un salariu de " << getSalariu() << "." << std::endl;
    }

    int getSalariu() {
        return m_salariu;
    }

    int getVechime() {
        return m_vechime;
    }
private:
    int m_salariu;
    int m_vechime;
};

class Forma {
public:
    virtual float getArie() = 0;
};

class Cerc : public Forma {
public:
    Cerc(int raza) : m_raza(raza) { }
    float getArie() override {
        return PI * (m_raza*m_raza);
    }
private:
    Cerc() { }
    int m_raza;
};

class Paralelipiped : public Forma {
public:
    float getArie() override {
        return m_lungime * m_latime;
    }
protected:
    Paralelipiped(const Paralelipiped& p) { }
    Paralelipiped(int lungime, int latime) : m_lungime(lungime), m_latime(latime) { }
private:
    Paralelipiped() { }
    int m_lungime;
    int m_latime;
};

class Patrat : public Paralelipiped {
public:
    Patrat(int lungime) : Paralelipiped(lungime, lungime) { }
private:
    Patrat() : Paralelipiped(0,0) { }
};

class Dreptunghi : public Paralelipiped {
public:
    Dreptunghi(int lungime, int latime) : Paralelipiped(lungime, latime) { }
private:
    Dreptunghi() : Paralelipiped(0,0) { }
};

class Telefon {
protected:
    Telefon(std::string telefon) : m_telefon(telefon) { }
    Telefon(const Telefon& t) : m_telefon(t.m_telefon) { }
    virtual void print() {
        std::cout << "Numar de telefon: " << m_telefon << std::endl;
    }
private:
    Telefon() { }
    std::string m_telefon;
};

class TelefonFix : public Telefon {
public:
    TelefonFix(std::string telefon) : Telefon(telefon) { }
    TelefonFix(const Telefon& t) : Telefon(t) { }
    void print() override {
        std::cout << "Telefonul este fix. ";
        Telefon::print();
    }
private:
    TelefonFix() : Telefon("") { }
};

class TelefonMobil : public Telefon {
public:
    TelefonMobil(std::string telefon) : Telefon(telefon) { }
    TelefonMobil(const Telefon& t) : Telefon(t) { }
    void print() override {
        std::cout << "Telefonul este mobil. ";
        Telefon::print();
    }
private:
    TelefonMobil() : Telefon("") { }
};

int main(int argc, char** argv) {

    return 0;
}