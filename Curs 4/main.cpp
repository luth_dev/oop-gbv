#include <iostream>
#include <vector>
#include <cstring>

class Polinom {
public:
    Polinom(std::vector<int> vec) : m_size(vec.size()) {
        for (int i = 0; i < m_size; ++i)
            m_data.push_back(vec[i]);
    }
    Polinom operator+(const Polinom& p) {
        Polinom tmp;
        if (m_size >= p.m_size)
            tmp.m_size = m_size;
        else
            tmp.m_size = p.m_size;
        for (int i = 0; i < tmp.m_size; ++i) {
            int f, l;
            _getFromIndex(i, *this, f);
            _getFromIndex(i, p, l);
            tmp.m_data.push_back(f+l);
        }
        return tmp;
    }
    Polinom operator*(const Polinom& p) {
        Polinom tmp;
        if (m_size >= p.m_size)
            tmp.m_size = m_size;
        else
            tmp.m_size = p.m_size;
        for (int i = 0; i < tmp.m_size; ++i) {
            int f, l;
            _getFromIndex(i, *this, f);
            _getFromIndex(i, p, l);
            tmp.m_data.push_back(f*l);
        }
        
        return tmp;
    }
    void print() {
        for (int i = 0; i < m_size; ++i) {
            std::cout << "x^" << m_size-1-i << "*" << m_data.at(i);
            if (i < m_size-1)
            std::cout << " + ";
        }
        std::cout << std::endl;
    }
private:
    Polinom() { }
    void _getFromIndex(int index, const Polinom& p, int& var) {
            try {
                var = p.m_data.at(index);
            }
            catch (std::out_of_range& /*e*/) {
                var = 0;
            }
    }
    int m_size;
    std::vector<int> m_data;
};

class Matrice {
public:
    Matrice(const Matrice& m) {
        m_ln = m.m_ln;
        m_col = m.m_col;
        _allocData();
        for (int i = 0; i < m_ln; ++i)
            for (int j = 0; j < m_col; ++j)
                m_data[i][j] = m.m_data[i][j];
    }
    Matrice(int lines, int columns, std::initializer_list<int> list) : m_ln(lines), m_col(columns) {
        _allocData();
        auto it = list.begin();
        for (int i = 0; i < m_ln; ++i)
            for (int j = 0; j < m_col; ++j) {
                if (it == list.end()) {
                    m_data[i][j] = 0;
                    continue;
                }
                m_data[i][j] = *it;
                ++it;
            }
    }
    Matrice(int lines, int columns, int val = 0) : m_ln(lines), m_col(columns) {
        _allocData();
        for (int i = 0; i < m_ln; ++i)
            for (int j = 0; j < m_col; ++j)
                m_data[i][j] = val;
    }

    void print() {
        for (int i = 0; i < m_ln; ++i) {
            for (int j = 0; j < m_col; ++j)
                std::cout << m_data[i][j] << " ";
            std::cout << std::endl;
        }
    }

    Matrice operator+(const Matrice& m) {
        if ((m_col != m.m_col) || (m_ln != m.m_ln)) {
            return Matrice(1,1,0);
        }
        Matrice tmp(m_ln, m_col);
        for (int i = 0; i < m_ln; ++i)
            for (int j = 0; j < m_col; ++j)
                tmp.m_data[i][j] = m_data[i][j] + m.m_data[i][j];
        return tmp;
    }

    Matrice operator*(Matrice& m) {
        if (m_col != m.m_ln)
            return Matrice(1,1,0);
        Matrice tmp(m_ln, m.m_col);
        int k = 0;
        for (int i = 0; i < tmp.m_ln; ++i) {
            for (int j = 0; j < tmp.m_col; ++j)
                for (int k = 0; k < m_col; ++k)
                tmp.m_data[i][j] += m_data[i][k] * m.m_data[k][j];
        }
        return tmp;
    }
private:

    void _allocData() {
        m_data = new int*[m_ln];
        for (int i = 0; i < m_ln; ++i)
            m_data[i] = new int[m_col];
    }
    int** m_data;
    int m_ln;
    int m_col;
};

int main(int argc, char** argv) {
    Matrice a(4, 2, {3, 3, 3, 3, 3, 3, 3, 3});
    Matrice b(2, 3, {1, 3, 3, 1, 1, 1});
    Matrice x(4, 2, {1, 3, 1, 3, 1, 3, 1, 3});
    Matrice c(a+x);
    Matrice d(a*b);
    c.print();
    d.print();
    return 0;
}