#include <iostream>
#include <unistd.h>

#define ALLOC_IN_BYTES 1024*1024*50

int allocMore(char* ptr) {
    ptr = new char[ALLOC_IN_BYTES];
    for (int i = 0; i < ALLOC_IN_BYTES; ++i)
        ptr[i] = 'A';
    ptr[ALLOC_IN_BYTES-1] = '\0';
    return ALLOC_IN_BYTES;
}

int main(int argc, char** argv) {
    char* hehe;
    int totalb;
    for (int i = 20; i >= 0; --i) {
        totalb += allocMore(hehe);
        sleep(1);
    }
    for (int i = 0; i < 1024*1024*10; ++i)
        std::cout << hehe;
}