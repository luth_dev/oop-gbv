#include <iostream>
#include <stdexcept>
#include <queue>

class MaxSizeException : public std::exception {
public:
    virtual const char* what() const noexcept { return "Max size reached!"; }
};

template <class T>
class Multime {
public:
    Multime(std::initializer_list<T> list, int maxSize = 100) : m_size(0), m_maxSize((maxSize > 100 ? 200 : 100)) {
        m_data = new T[m_maxSize];
        for (typename std::initializer_list<T>::iterator it = list.begin(); it != list.end(); ++it)
            insert(*it);
    }

    Multime(const Multime &m) {
        m_maxSize = m.m_maxSize;
        m_data = new T[m_maxSize];
        for (int i = 0; i < m_maxSize; ++i)
            m_data[i] = m.m_data[i];
    }

    bool insert(const T e) {
        if (m_size >= m_maxSize) {
            throw MaxSizeException();
        }
        for (int i = 0; i < m_size; ++i) {
            if (m_data[i] == e)
                 return false;
        }
        m_data[m_size] = e;
        ++m_size;
        return true;
    }

    bool remove(T e) {
        for (int i = 0; i < m_size; ++i) {
            if (m_data[i] == e) {
                for (int j = i; j < m_size-1; ++j) {
                    m_data[j] = m_data[j+1];
                }
                --m_size;
                return true;
            }
        }
        return false;
    }

    void print() {
        if (m_size == 0) {
            std::cout << "Multimea este goala" << std::endl;
            return;
        }
        for (int i = 0; i < m_size; ++i)
            std::cout << m_data[i] << " ";
        std::cout << std::endl;
    }

    bool contains(T e) const {
        for (int i = 0; i < m_size; ++i) {
            if (m_data[i] == e)
                return true;
        }
        return false;
    }

    template <class U>
    friend Multime<U> operator+(const Multime<U> &a, const Multime<U> &b);
    template <class U>
    friend Multime<U> operator*(const Multime<U> &a, const Multime<U> &b);
private:
    Multime(int maxSize) : m_maxSize(maxSize), m_size(0) { 
        m_data = new T[maxSize];
    }
    T* m_data;
    int m_maxSize;
    int m_size;
};

template <class T>
Multime<T> operator+(const Multime<T> &a, const Multime<T> &b) {
    int maxSize = a.m_size + b.m_size;
    Multime<T> temp(maxSize);
    for (int i = 0; i < a.m_size; ++i)
        temp.insert(a.m_data[i]);
    for (int i = 0; i < b.m_size; ++i)
        temp.insert(b.m_data[i]);
    return temp;
}

template <class T>
Multime<T> operator*(const Multime<T> &a, const Multime<T> &b) {
    int maxSize = a.m_size + b.m_size;
    Multime<T> temp(maxSize);
    for (int i = 0; i < a.m_size; ++i)
        if (b.contains(a.m_data[i]))
            temp.insert(a.m_data[i]);
    for (int i = 0; i < b.m_size; ++i)
        if (a.contains(b.m_data[i]))
            temp.insert(b.m_data[i]);
    return temp;
}

template <class T>
class BinaryNode {
public:
    BinaryNode() : m_data(nullptr), m_left(nullptr), m_right(nullptr) { }
    void SetData(const T &data) {
        if (m_data == nullptr)
            m_data = new T;
        *m_data = data;
    }
    void SetLeft(const T &data) {
        if (m_left == nullptr)
            m_left = new BinaryNode;
        m_left->SetData(data);
    }
    void SetRight(const T &data) {
        if (m_right == nullptr)
            m_right = new BinaryNode;
        m_right->SetData(data);
    }
    T* GetData() {
        return m_data;
    }
    BinaryNode<T>* GetLeft() {
        return m_left;
    }
    BinaryNode<T>* GetRight() {
        return m_right;
    }
    ~BinaryNode() {
        delete m_data;
        delete m_left;
        delete m_right;
    }
private:
    T *m_data;
    BinaryNode<T> *m_left;
    BinaryNode<T> *m_right;
};

template <class T>
class BinaryNodeQueue {
public:
    void Enqueue(BinaryNode<T> *e) {
        if (e == nullptr)
            return;
        m_queue.push(e);
    }
    void Dequeue() {
        m_queue.pop();
    }
    bool IsEmpty() {
        return (m_queue.size() == 0);
    }
    BinaryNode<T>* Front() {
        return m_queue.front();
    }
private:
    std::queue<BinaryNode<T>*> m_queue;
};
 
template <class T>
class CompleteBinaryTree {
public:
    CompleteBinaryTree() {

    }
    void Insert(const T &e) {
        BinaryNodeQueue<T> QueueMgr;
        BinaryNode<T>* currentNode;
        QueueMgr.Enqueue(root);
        while(true) {
            currentNode = QueueMgr.Front();
            if (currentNode->GetData() == nullptr) {
                currentNode->SetData(e);
                break;
            }
            QueueMgr.Enqueue(currentNode->GetLeft());
            QueueMgr.Enqueue(currentNode->GetRight());
            QueueMgr.Dequeue();
        }
    }

    void Print(BinaryNode<T>* node) {
        if (node == nullptr)
            return;
        std::cout << node->GetData();
        Print(node->GetLeft());
        Print(node->GetRight());
    }

    BinaryNode<T>* GetRoot() {
        return root;
    }

    ~CompleteBinaryTree() {
        delete root;
    }
private:
    BinaryNode<T> *root;
};

using namespace std;

void Test(BinaryNode<int> *e) {
    if (e->GetData() == nullptr)
        cout << "Data is nullptr";
    else
        cout << "Data: " << *(e->GetData());
    cout << endl;
    if (e->GetLeft() == nullptr)
        cout << "Left is nullptr";
    else
        cout << "Left data is " << *(e->GetLeft()->GetData());
    cout << endl;
    if (e->GetRight() == nullptr)
        cout << "Right is nullptr";
    else
        cout << "Right data is " << *(e->GetRight()->GetData());
    cout << endl;
}

int main(int argc, char** argv) {
    // Multime<int> a({1, 2, 3});
    // Multime<int> b({3, 5});
    // Multime<int> c(a+b);
    // Multime<int> d(a*b);
    // std::cout << "Multimea A: ";
    // a.print();
    // std::cout << "Multimea B: ";
    // b.print();
    // std::cout << "A reunit cu B: ";
    // c.print(); 
    // std::cout << "A intersectat cu B: ";
    // d.print();
    // Multime<char> a({'a', 'b', 'd'});
    // Multime<char> b({'d', 'e', 'x'});
    // Multime<char> c(a+b);
    // Multime<char> d(a*b);
    // std::cout << "Multimea A: ";
    // a.print();
    // std::cout << "Multimea B: ";
    // b.print();
    // std::cout << "A reunit cu B: ";
    // c.print(); 
    // std::cout << "A intersectat cu B: ";
    // d.print();
    // CompleteBinaryTree<int> hehe;
    // hehe.Insert(1);
    // hehe.Insert(3);
    // hehe.Insert(10);
    // hehe.Insert(4);
    // hehe.Print(hehe.GetRoot());
    BinaryNode<int> *x = new BinaryNode<int>;
    Test(x);
    x->SetData(3);
    x->SetLeft(5);
    Test(x);
    return 0;
}