#include <iostream>

class Animal{
public:
    virtual void doSomething() = 0;
    virtual void whoami() = 0;
};

class Mamifer : virtual public Animal {
public:
    void whoami() override {
        std::cout << "Sunt un mamifer" << std::endl;
    }
    void doSomething() override {
        std::cout << "Sunete de mamifer (?)" << std::endl;
    }
};

class AnimalZburator : virtual public Animal {
public:
    void whoami() override {
        std::cout << "Sunt un animal zburator" << std::endl;
    }
    void doSomething() override {
        std::cout << "*flies*" << std::endl;
    }
};

class Liliac : public Mamifer, public AnimalZburator {
public:
    void doSomething() override {
        std::cout << "*sunete de liliac*" << std::endl;
    }

    void whoami() override {
        std::cout << "Sunt un liliac" << std::endl;
    }
};

class Carte {
public:
    Carte(std::string titlu, std::string autor, int an) : m_titlu(titlu), m_autor(autor), m_an(an) { }
    Carte(const Carte& c) : m_titlu(c.m_titlu), m_autor(c.m_autor), m_an(c.m_an) { }
    virtual void print() = 0;
protected:
    std::string m_titlu;
    std::string m_autor;
    int m_an;
};

class Roman : public Carte {
public:
    Roman(std::string titlu, std::string autor, int an, int volum = 1) : Carte(titlu, autor, an), m_volum(volum) { }
    Roman(const Roman& r) : Carte(r), m_volum(r.m_volum) { }
    void print() override {
        std::cout << "Romanul " << m_titlu  << " volumul " << m_volum <<  " este scris de catre " << m_autor << " a fost publicat in anul " << m_an << std::endl;
    }
private:
    int m_volum;
};

class BandaDesenata : public Carte {
public:
    BandaDesenata(std::string titlu, std::string autor, int an, int episod = 1) : Carte(titlu, autor, an), m_episod(episod) { }
    BandaDesenata(const BandaDesenata& b) : Carte(b), m_episod(b.m_episod) { }
    void print() override {
        std::cout << "Banda desenata " << m_titlu << " episodul " << m_episod << " a aparut in anul " << m_an << ". Autor: " << m_autor << std::endl;
    }
private:
    BandaDesenata();
    int m_episod;
};

class CarteSpecialitate : public Carte {
public:
    CarteSpecialitate(std::string titlu, std::string autor, int an, std::string domeniu) : Carte(titlu, autor, an), m_domeniu(domeniu) { }
    CarteSpecialitate(const CarteSpecialitate& c) : Carte(c), m_domeniu(c.m_domeniu) { }
    void print() override {
        std::cout << "Cartea " << m_titlu << " de specialitatea " << m_domeniu << " scrisa de " << m_autor << " aparuta in anul " << m_an << std::endl;
    }
private:
    CarteSpecialitate();
    std::string m_domeniu;
};

int main(int argc, char** argv) {
    // Animal* a = new Mamifer;
    // Animal* liliac = new Liliac;
    // a->whoami();
    // a->doSomething();
    // liliac->whoami();
    // liliac->doSomething();
    Carte* a = new Roman("Hehexd", "Denis", 1993, 3);
    Carte* b = new BandaDesenata("Hyperion", "Alexander", 2018, 11);
    Carte* c = new CarteSpecialitate("C/C++", "Nicolae Constantinescu", 2010, "Informatica");
    a->print();
    b->print();
    c->print();
    return 0;
}