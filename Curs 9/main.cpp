#include <iostream>
#include <cstring>
#include <cstdlib>

class ExceptiiGreutate {
public:
    ExceptiiGreutate(std::string str) {
        msg = str;
    }
    virtual std::string what() = 0;
protected:
    std::string msg;
};

class DepasireGreutate : public ExceptiiGreutate{
public:
    DepasireGreutate(std::string str) : ExceptiiGreutate(str) { }

    std::string what() {
        return std::string(msg);
    }
};

class AvertismentDepasireGreutate : public ExceptiiGreutate {
public:
    AvertismentDepasireGreutate(std::string str) : ExceptiiGreutate(str) { }
    std::string what() {
        return std::string(msg);
    }
};

class Cantar {
public:
    Cantar(int max) : m_greutateMax(max) { }
    int cantarire(int x) {
        if (x > (m_greutateMax + m_greutateMax * 0.1))
            throw DepasireGreutate("[GREUTATE MAXIMA DEPASITA]");
        else if(x > m_greutateMax)
            throw AvertismentDepasireGreutate("[LIMITA GREUTATE DEPASITA]");
        else
            return x;
    }
private:
    Cantar();
    int m_greutateMax;
};

class StackException {
public:
    StackException(std::string str) : msg(str) { }
    std::string what() const {
        return msg;
    }
private:
    const std::string msg;
};

template <class T>
class Stack {
public:
    Stack(int size) : m_size(size), m_ptr(0) {
        if (m_size <= 0)
            throw StackException("Stack size can't be <= 0");
        m_data = new T[m_size];
    } 
    void push(const T& e) {
        if (m_ptr == m_size) {
            throw StackException("Stack overflow.");
        }
        m_data[m_ptr] = e;
        ++m_ptr;
    }
    T top() {
        if (m_ptr == 0)
            throw StackException("Stack is empty.");
        return m_data[m_ptr-1];
    }
    void pop() {
        if (m_ptr == 0)
            throw StackException("Stack is empty.");
        --m_ptr;
    }
    void empty() {
        if (m_ptr == 0)
            throw StackException("Stack is empty.");
        m_ptr = 0;
    }
private:
    int m_ptr;
    const int m_size;
    T* m_data;
};

int main(int agc, char** argv) {
    Cantar c(1000);
    try {
        std::cout << c.cantarire(111) << " kg" << std::endl;
    }
    catch (DepasireGreutate& e) {
        std::cout << e.what() << std::endl;
    }
    catch (AvertismentDepasireGreutate& e) {
        std::cout << e.what() << std::endl;
    }
    Stack<int> s(0);
    try {
        s.push(3);
        s.push(2);
        std::cout << "Stack top: " << s.top() << std::endl;
        s.push(5);
        s.empty();
        s.pop();
    }
    catch (StackException &e) {
        std::cout << e.what() << std::endl;
    }
    return 0;
}