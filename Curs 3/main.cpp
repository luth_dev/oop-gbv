#include <iostream>

class Matrice {
public:
    Matrice(int lines, int colums) : m_ln(lines), m_col(colums) {
        m_data = new int[m_ln * m_col];
    }

    ~Matrice() {
        delete[] m_data;
    }

    int& operator()(int line, int column) {
        return m_data[line * m_col + column];
    }
    int* m_data;
private:
    int m_ln;
    int m_col;
    
};

enum class FormatData {
    TEXT,
    NUMERIC
};

class DateCalendaristice {
public:
    DateCalendaristice(int zi, int luna, int an) : m_zi(zi), m_luna(luna), m_an(an) { }

    void print(FormatData f) {
        std::string tmp;
        tmp.append(std::to_string(m_zi)); 
        switch(f) {
            case FormatData::TEXT: tmp.append(" " + monthToString(m_luna) + " "); break;
            case FormatData::NUMERIC: tmp.append("." + std::to_string(m_luna) + "."); break;
            default: return;
        }
        tmp.append(std::to_string(m_an));
        std::cout << tmp;
    }
private:

    std::string monthToString(int luna) {
        switch (luna) {
            case 1: return "Ianuarie";
            case 2: return "Februarie";
            case 3: return "Martie";
            case 4: return "Aprilie";
            case 5: return "Mai";
            case 6: return "Iunie";
            case 7: return "Iulie";
            case 8: return "August";
            case 9: return "Septembrie";
            case 10: return "Octombrie";
            case 11: return "Noiembrie";
            case 12: return "Decembrie";
            default: return "Invalid";
        }
    }
    int m_zi;
    int m_luna;
    int m_an;
};

int main(int argc, char** argv) {
    DateCalendaristice a(31, 1, 1999);
    a.print(FormatData::NUMERIC);
    std::cout << std::endl;
    a.print(FormatData::TEXT);
    std::cout << std::endl;
}